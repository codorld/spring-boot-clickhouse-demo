代码: [点击跳转gitee库](https://gitee.com/codorld/spring-boot-clickhouse-demo.git)

整合springboot+mybatis plus的使用过程与mysql/pgsql类似, 配置分页插件, 自动填充createTime/updateTime等是一样的使用

数据库部分的sql语法与常用的sql语法类似

- 创建表(使用sql, 具体类型可以参考图形界面中的内容)
  ```sql
  --drop table example_table;
  CREATE TABLE example_table (
  --    id UUID DEFAULT generateUUIDv4(),
      id String,
      equipment_num String,
      equipment_type String,
      log_type String,
      log_content String,
      create_time DateTime64(3)
  ) ENGINE = MergeTree()
  ORDER BY (create_time)
  PRIMARY KEY (create_time);
  ```
  因为大多使用clickhouse来做日志存储, 一般都是来降序获取数据, 所以使用`create_time`来排序, 查找时候会快一点
    - `MergeTree`: 存储引擎
    - `PRIMARY KEY`: 主键, clickhouse中主键没有单一约束, 也即可重复
    - `ORDER BY`: 指明排序字段
    - `id String`: clickhouse不可以实现12345这样的递增id, 所以采用字符串存储, 在mp中使用`@TableId(type = IdType.ASSIGN_UUID)`配合生成
- 整合springboot+mybatis-plus
    - yml中driverClassName: 不会自动识别, 需要配置`com.clickhouse.jdbc.ClickHouseDriver`
    - yml中url: `jdbc:clickhouse://localhost:8123/default?useTimeZone=true&useServerTimeZone=false&serverTimeZone=UTC`
    - 分页插件修改: `PaginationInnerInterceptor innerInterceptor = new PaginationInnerInterceptor(DbType.CLICK_HOUSE);`
    - 查插: 已经测试过了, 9千万条数据的条件筛选和分页(第1百万页, 每页20条耗时100毫秒左右)
    - 删改: 只测试了`***ById`发现是可以用的, 如果有需要时候, 在额外测试其他的情况能否适配, 建议参考官网
- 参考:
    - [github clickhouse](https://github.com/ClickHouse/ClickHouse/)
    - [官网文档](https://clickhouse.com/docs/zh)
    - [Clickhouse学习整理](https://blog.csdn.net/Foools/article/details/123698613)
    - [ClickHouse学习笔记（一）](https://blog.csdn.net/qq_40378034/article/details/120256757)
    - [ClickHouse学习笔记（二）](https://blog.csdn.net/qq_40378034/article/details/120473116)
    - [大数据技术之ClickHouse](https://daiqiaohong.gitee.io/blog/%E5%A4%A7%E6%95%B0%E6%8D%AE%E6%8A%80%E6%9C%AF%E4%B9%8BClickHouse/)
    - [clickhouse 实现序号自增(非主键)](https://blog.csdn.net/weixin_45077799/article/details/125332722)
    - [clickhouse字段自增解决办法](https://blog.csdn.net/qq_41110377/article/details/128634788)
    - [ClickHouse数据库数据定义手记之数据类型](https://www.cnblogs.com/throwable/p/14018954.html)
    - [clickhouse数据库使用jdbc存储毫秒和纳秒](https://blog.csdn.net/gsls200808/article/details/125488177)