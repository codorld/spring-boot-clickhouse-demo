package com.clickhouse.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.clickhouse.demo.entity.ExampleTable;

/**
 * @author codor
 * @date 2023/08/17 11:36
 */
public interface ExampleTableService extends IService<ExampleTable> {
}
