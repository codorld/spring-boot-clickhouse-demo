package com.clickhouse.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.clickhouse.demo.entity.ExampleTable;
import com.clickhouse.demo.mapper.ExampleTableMapper;
import com.clickhouse.demo.service.ExampleTableService;
import org.springframework.stereotype.Service;

/**
 * @author codor
 * @date 2023/08/17 11:36
 */
@Service
public class ExampleTableServiceImpl
        extends ServiceImpl<ExampleTableMapper, ExampleTable>
        implements ExampleTableService {
}
