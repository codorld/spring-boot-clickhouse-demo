package com.clickhouse.demo.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

/**
 * @author codor
 * @date 2023/08/17 11:28
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ExampleTable {

    @TableId(type = IdType.ASSIGN_UUID)
    String id;

    String equipmentNum;

    String equipmentType;

    String logType;

    String logContent;

    @TableField(fill = FieldFill.INSERT)
    LocalDateTime createTime;

    @JsonIgnore
    @TableField(exist = false)
    Integer num;
}
