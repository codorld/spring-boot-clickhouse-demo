package com.clickhouse.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.clickhouse.demo.entity.ExampleTable;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author codor
 * @date 2023/08/17 11:35
 */
@Mapper
public interface ExampleTableMapper extends BaseMapper<ExampleTable> {
}
